﻿/*
 * Projet      : ANDRIVETT_ParkingMeter
 * Auteur      : Tom Andrivet
 * Date        : 18.03.2021
 * Description : Application de calcul du coût d'un stationnement et d’impression d’un ticket
 * Version     : 1.0
 */
using System;

namespace ParkingMeter
{
    public class Ticket
    {
        /*
         * Champs privés relié aux propriétées
         */
        private readonly string _vehicule;
        private readonly DateTime _begin;
        private readonly TimeSpan _duration;
        private readonly DateTime _end;
        private readonly decimal _cost;

        
        /// <summary>
        /// Type de vehicule
        /// </summary>
        public string Vehicule => _vehicule;

        /// <summary>
        /// Date de début de stationnement
        /// </summary>
        public DateTime Begin => _begin;

        /// <summary>
        /// Durée du stationnement
        /// </summary>
        public TimeSpan Duration => _duration;

        /// <summary>
        /// Date de fin de stationnement
        /// </summary>
        public DateTime End => _end;

        /// <summary>
        /// Coût du stationnement
        /// </summary>
        public decimal Cost => _cost;

        /// <summary>
        /// Constructeur principal
        /// </summary>
        /// <param name="pVehiculeType">Type du vehicule</param>
        /// <param name="pBegin">Date de début de stationnement</param>
        /// <param name="pDuration">Durée du stationnement</param>
        /// <param name="pEnd">Date de fin de stationnement</param>
        /// <param name="pCost">Coût du stationnement</param>
        public Ticket(string pVehiculeType, DateTime pBegin, TimeSpan pDuration, DateTime pEnd, decimal pCost)
        {
            _vehicule = pVehiculeType;
            _begin = pBegin;
            _duration = pDuration;
            _end = pEnd;
            _cost = pCost;
        }
    }
}