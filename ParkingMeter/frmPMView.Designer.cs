﻿namespace ParkingMeter
{
    partial class frmPMView
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTypeVehicule = new System.Windows.Forms.Label();
            this.cbxTypeVehicule = new System.Windows.Forms.ComboBox();
            this.lblRegistration = new System.Windows.Forms.Label();
            this.tbxRegistration = new System.Windows.Forms.TextBox();
            this.lblDuration = new System.Windows.Forms.Label();
            this.cbxDuration = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblTicket = new System.Windows.Forms.Label();
            this.tbxTicket = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblTypeVehicule
            // 
            this.lblTypeVehicule.Location = new System.Drawing.Point(12, 22);
            this.lblTypeVehicule.Name = "lblTypeVehicule";
            this.lblTypeVehicule.Size = new System.Drawing.Size(123, 23);
            this.lblTypeVehicule.TabIndex = 0;
            this.lblTypeVehicule.Text = "Type de véhicule";
            // 
            // cbxTypeVehicule
            // 
            this.cbxTypeVehicule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxTypeVehicule.FormattingEnabled = true;
            this.cbxTypeVehicule.Location = new System.Drawing.Point(141, 19);
            this.cbxTypeVehicule.Name = "cbxTypeVehicule";
            this.cbxTypeVehicule.Size = new System.Drawing.Size(166, 24);
            this.cbxTypeVehicule.TabIndex = 1;
            // 
            // lblRegistration
            // 
            this.lblRegistration.Location = new System.Drawing.Point(12, 85);
            this.lblRegistration.Name = "lblRegistration";
            this.lblRegistration.Size = new System.Drawing.Size(123, 23);
            this.lblRegistration.TabIndex = 2;
            this.lblRegistration.Text = "Immatriculation";
            // 
            // tbxRegistration
            // 
            this.tbxRegistration.Location = new System.Drawing.Point(141, 82);
            this.tbxRegistration.Name = "tbxRegistration";
            this.tbxRegistration.Size = new System.Drawing.Size(166, 22);
            this.tbxRegistration.TabIndex = 3;
            // 
            // lblDuration
            // 
            this.lblDuration.Location = new System.Drawing.Point(12, 148);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(123, 23);
            this.lblDuration.TabIndex = 4;
            this.lblDuration.Text = "Durée [min]";
            // 
            // cbxDuration
            // 
            this.cbxDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDuration.FormattingEnabled = true;
            this.cbxDuration.Location = new System.Drawing.Point(141, 145);
            this.cbxDuration.Name = "cbxDuration";
            this.cbxDuration.Size = new System.Drawing.Size(166, 24);
            this.cbxDuration.TabIndex = 5;
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.btnOK.Location = new System.Drawing.Point(141, 212);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(166, 61);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblTicket
            // 
            this.lblTicket.Location = new System.Drawing.Point(346, 186);
            this.lblTicket.Name = "lblTicket";
            this.lblTicket.Size = new System.Drawing.Size(100, 23);
            this.lblTicket.TabIndex = 7;
            this.lblTicket.Text = "Ticket";
            // 
            // tbxTicket
            // 
            this.tbxTicket.Font = new System.Drawing.Font("Consolas", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.tbxTicket.Location = new System.Drawing.Point(346, 212);
            this.tbxTicket.Multiline = true;
            this.tbxTicket.Name = "tbxTicket";
            this.tbxTicket.ReadOnly = true;
            this.tbxTicket.Size = new System.Drawing.Size(336, 141);
            this.tbxTicket.TabIndex = 8;
            // 
            // frmPMView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 366);
            this.Controls.Add(this.tbxTicket);
            this.Controls.Add(this.lblTicket);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cbxDuration);
            this.Controls.Add(this.lblDuration);
            this.Controls.Add(this.tbxRegistration);
            this.Controls.Add(this.lblRegistration);
            this.Controls.Add(this.cbxTypeVehicule);
            this.Controls.Add(this.lblTypeVehicule);
            this.Name = "frmPMView";
            this.Text = "Parcmètre V1";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.ComboBox cbxTypeVehicule;
        private System.Windows.Forms.ComboBox cbxDuration;
        private System.Windows.Forms.Label lblTypeVehicule;
        private System.Windows.Forms.Label lblRegistration;
        private System.Windows.Forms.Label lblDuration;
        private System.Windows.Forms.Label lblTicket;
        private System.Windows.Forms.TextBox tbxRegistration;
        private System.Windows.Forms.TextBox tbxTicket;

        #endregion
    }
}