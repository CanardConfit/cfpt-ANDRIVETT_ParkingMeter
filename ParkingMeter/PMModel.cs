﻿/*
 * Projet      : ANDRIVETT_ParkingMeter
 * Auteur      : Tom Andrivet
 * Date        : 18.03.2021
 * Description : Application de calcul du coût d'un stationnement et d’impression d’un ticket
 * Version     : 1.0
 */
using System;
using System.Collections.Generic;

namespace ParkingMeter
{
    public class PMModel
    {
        /*
         * Champs privés relié aux propriétées
         */
        private readonly List<VehiculeRate> _vehiculeRates;
        private readonly List<int> _availableDurations;
        private VehiculeRate _currentVehiculeRate;

        /// <summary>
        /// Liste de <see cref="VehiculeRate"/> contenant les types de vehicule supporté ainsi que le coût du stationnement pour chacun
        /// </summary>
        public List<VehiculeRate> VehiculeRates => _vehiculeRates;

        /// <summary>
        /// Liste des durée de stationnement possible (en minute)
        /// </summary>
        public List<int> AvailableDurations => _availableDurations;

        /// <summary>
        /// Vehicule actuellement traité
        /// </summary>
        private VehiculeRate CurrentVehiculeRate
        {
            get => _currentVehiculeRate;
            set => _currentVehiculeRate = value;
        }

        /// <summary>
        /// Constructeur principal
        /// </summary>
        public PMModel()
        {
            _currentVehiculeRate = null;
            
            /*
             * Initialisation des vehicules supporté et de leurs coût de stationnement
             */
            _vehiculeRates = new List<VehiculeRate>();
            _vehiculeRates.AddRange(new []
            {
                new VehiculeRate("Moto", 0.75M),
                new VehiculeRate("Voiture", 1M),
                new VehiculeRate("Camionnette", 1.5M)
            });

            /*
             * Initialisation des durée de stationnement acceptées
             */
            _availableDurations = new List<int>();
            _availableDurations.AddRange(new []
            {
                15,
                30,
                45,
                60,
                75,
                90,
                105,
                120
            });
        }

        /// <summary>
        /// Fonction qui permet de générer un ticket de stationnement
        /// </summary>
        /// <param name="pVehiculeIndex">Index de la liste <see cref="VehiculeRates"/> pour le type de vehicule</param>
        /// <param name="pDurationIndex">Index de la liste <see cref="AvailableDurations"/> pour la durée choisie</param>
        /// <returns>Une instance de <see cref="Ticket"/></returns>
        public Ticket GenerateTicket(int pVehiculeIndex, int pDurationIndex)
        {
            // Verification que les index sont possible dans leurs liste respectif
            if (_vehiculeRates.Count > pVehiculeIndex && AvailableDurations.Count > pDurationIndex)
            {
                /*
                 * Récuperation de la durée choisi et du VehiculeRate correspondant aux indexs
                 */
                int duration = AvailableDurations[pDurationIndex];
                CurrentVehiculeRate = VehiculeRates[pVehiculeIndex];
                
                // Definition de la date de début de stationnement (Maintenant)
                DateTime begin = DateTime.Now;
                
                // Calcul du coût à payer pour le stationnement
                decimal cost = CurrentVehiculeRate.ComputeParkingCost(begin, AvailableDurations[pDurationIndex]);
                
                // Retour de l'instance du ticket avec les valeurs calculées
                return new Ticket(CurrentVehiculeRate.Name,begin, new TimeSpan(0, duration, 0), begin.AddMinutes(duration), cost);
            }

            // Retour par défaut
            return null;
        }
    }
}