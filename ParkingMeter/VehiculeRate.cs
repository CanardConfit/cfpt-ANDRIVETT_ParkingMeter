﻿/*
 * Projet      : ANDRIVETT_ParkingMeter
 * Auteur      : Tom Andrivet
 * Date        : 18.03.2021
 * Description : Application de calcul du coût d'un stationnement et d’impression d’un ticket
 * Version     : 1.0
 */
using System;

namespace ParkingMeter
{
    public class VehiculeRate
    {
        /*
         * Champs privés relié aux propriétées
         */
        private string _name;
        private decimal _costFor15Min;

        /// <summary>
        /// Type de vehicule
        /// </summary>
        public string Name => _name;

        /// <summary>
        /// Coût du stationnement pour 15 minutes
        /// </summary>
        public decimal CostFor15Min => _costFor15Min;

        /// <summary>
        /// Constructeur principal
        /// </summary>
        /// <param name="pName">Type de vehicule pour ce tarif</param>
        /// <param name="pCostFor15Min">Coût du stationnement pour 15min</param>
        public VehiculeRate(string pName, decimal pCostFor15Min)
        {
            _name = pName;
            _costFor15Min = pCostFor15Min;
        }

        /// <summary>
        /// Fonction qui permet de calculer le prix du stationnement en fonction de la durée et de l'heure de début de stationnement
        /// </summary>
        /// <param name="pBegin">Date de début de stationnemet</param>
        /// <param name="pDuration">Durée du stationnement</param>
        /// <returns></returns>
        public decimal ComputeParkingCost(DateTime pBegin, int pDuration)
        {
            if (pBegin.Hour >= 19 || pBegin.Hour < 6)
                return 0M;

            return pDuration * _costFor15Min / 15;
        }
    }
}