﻿/*
 * Projet      : ANDRIVETT_ParkingMeter
 * Auteur      : Tom Andrivet
 * Date        : 18.03.2021
 * Description : Application de calcul du coût d'un stationnement et d’impression d’un ticket
 * Version     : 1.0
 */
using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace ParkingMeter
{
    public partial class frmPMView : Form
    {
        /*
         * Champs privés relié aux propriétées
         */
        private PMModel _model;
        
        /// <summary>
        /// Model ParcMetre
        /// </summary>
        private PMModel Model => _model;

        /// <summary>
        /// Constructeur principal
        /// </summary>
        public frmPMView()
        {
            InitializeComponent();
            
            // Instanciation du modèle ParcMetre
            _model = new PMModel();

            // Initilisation des combobox avec les valeurs du modèle
            cbxDuration.DataSource = _model.AvailableDurations;
            cbxTypeVehicule.DataSource = _model.VehiculeRates.Select(rate => rate.Name).ToList();
        }

        /// <summary>
        /// Fonction d'evenement quand le bouton <see cref="btnOK"/> est cliqué
        /// </summary>
        /// <param name="sender">Instance ayant envoyé l'evenement</param>
        /// <param name="e">Paramètres de l'evenement</param>
        private void btnOK_Click(object sender, EventArgs e)
        {
            // Message d'erreur si des champs ne sont pas remplis
            if (cbxTypeVehicule.SelectedItem == null || cbxDuration.SelectedItem == null ||
                tbxRegistration.Text == "")
            {
                MessageBox.Show("Un/des champs n'est/sont pas rempli(s) !", "Erreur", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }
            
            /*
             * Generation du ticket et affichage
             */
            Ticket ticket = _model.GenerateTicket(cbxTypeVehicule.SelectedIndex, cbxDuration.SelectedIndex);

            tbxTicket.Text = $"véhicule : {ticket.Vehicule}\r\n" +
                             $"immatriculation : {tbxRegistration.Text}\r\n" +
                             $"début : {ticket.Begin:dd.MM.yyyy HH:mm}\r\n" +
                             $"fin   : {ticket.End:dd.MM.yyyy HH:mm}\r\n" +
                             $"durée : {ticket.Duration.TotalMinutes} min\r\n" +
                             $"coût  : {ticket.Cost.ToString("0.00", CultureInfo.InvariantCulture)} CHF\r\n";
        }
    }
}