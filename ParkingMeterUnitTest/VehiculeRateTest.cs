/*
 * Projet      : ANDRIVETT_ParkingMeter
 * Auteur      : Tom Andrivet
 * Date        : 18.03.2021
 * Description : Application de calcul du coût d'un stationnement et d’impression d’un ticket
 * Version     : 1.0
 */
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParkingMeter;

namespace ParkingMeterUnitTest
{
    [TestClass]
    public class VehiculeRateTest
    {
        /**
         * Test qui verfie que les propriétées de la classe <see cref="VehiculeRate"/> sont bien remplie au constructeur.
         * -> Verification de Name.
         * -> Verification de CostFor15Min.
         */
        [TestMethod]
        public void Assert_Constructor_Fill_Properties()
        {
            string name = "Test Vehicule";
            decimal cost = 15M;
            VehiculeRate rate = new VehiculeRate(name, cost);
            
            Assert.AreEqual(name, rate.Name);
            Assert.AreEqual(cost, rate.CostFor15Min);
        }

        /**
         * Test qui verifie que la fonction <see cref="VehiculeRate.ComputeParkingCost"/> marche correctement
         * -> On ne paie rien après 19h.
         * -> On paie 3CHF avant 19h en voiture (45 minutes de parking).
         * -> On paie 2.25CHF avant 19h en moto (45 minutes de parking).
         */
        [TestMethod]
        public void Assert_Function_ComputeParkingCost_Work()
        {
            VehiculeRate voiture = new VehiculeRate("Voiture", 1.0M);
            VehiculeRate moto = new VehiculeRate("Moto", 0.75M);

            Assert.AreEqual(0M, voiture.ComputeParkingCost(new DateTime(2021, 03, 18, 20, 0, 0), 45));
            
            Assert.AreEqual(3M,voiture.ComputeParkingCost(new DateTime(2021, 03, 18, 18, 0, 0), 45));
            Assert.AreEqual(2.25M,moto.ComputeParkingCost(new DateTime(2021, 03, 18, 12, 0, 0), 45));
        }
    }
}